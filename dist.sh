#!/usr/bin/env sh
rm -R build 2>/dev/null
mkdir "build"
cp -R ./src/*.* ./build/
cp -R ./src/icons/ ./build/icons/
cd ./build/
zip -r ../build/build.zip ./
cd ../
rm -R dist 2>/dev/null
mkdir "dist"
mv ./build/build.zip ./dist/three-as-a-sidebar-firefox-X.XX.xpi
