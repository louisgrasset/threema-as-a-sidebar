# Threema Web as a Sidebar

## What it does

Allows it to run [Threema Web](https://web.threema.ch) as a sidebar on your browser. Fully inspired (copied :p) from [this work](https://github.com/soapdog/webextension-facebook-messenger-as-a-sidebar)

## Build
Simply clone the repo, ``cd`` in then run ``sh ./dist.sh``.
*Dependencies : you'll need the `zip` & `npm` command installed in your system.*